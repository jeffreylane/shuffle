package shuffle

import "math/rand"

// Shuffle expects the argument a of type A (slice of any)
//
// Does a simple shuffle of the deck, so to speak. It scales with the size of the deck.
func Shuffle[A ~[]E, E any](a A) A {
	if len(a) < 2 {
		// nothing to sort
		return a
	}
	// If it's a slice of two, flip a coin, and if heads just swap the values
	if len(a) == 2 {
		if RandomRange(0, 1, true) == 1 {
			return Swap(a, 0, 1)
		} else {
			return a
		}
	}
	// No need to split the deck with 3
	if len(a) == 3 {
		for {
			first := RandomRange(0, 2, true)
			second := RandomRange(0, 2, true)
			if first == second {
				continue
			} else {
				return Swap(a, first, second)
			}
		}
	}
	// Anything bigger and we'll split the deck then shuffle
	half := len(a) / 2
	if half%2 == 0 {
		half++
	}
	// For now, only iterating for half the deck size for no real reason.
	// The full deck just seemed overkill when I was first winging it on this func.
	// Need to look at this closer.
	for i := 0; i < half; i++ {
		a = Split(a)

		second := len(a) / 2
		first := RandomRange(0, second, false)
		second = RandomRange(second, len(a)-1, true)
		a = Swap(a, first, second)
	}
	return a
}

// Split "splits" the slice at the midpoint, like cutting a deck of cards
func Split[A ~[]E, E any](a A) A {
	if len(a) < 2 {
		// nothing to split
		return a
	}
	midPoint := len(a) / 2
	a1 := a[:midPoint]
	a2 := a[midPoint:]
	return append(a2, a1...)
}

// Swap swaps the slice values of the specified first and second positions
func Swap[A ~[]E, E any](a A, first, second int) A {
	temp := a[first]
	a[first] = a[second]
	a[second] = temp
	return a
}

// Returns a random number within the range of min and max, but never max unless includeMax is true.
func RandomRange(min, max int, includeMax bool) int {
	if includeMax {
		max++
	}
	return rand.Intn(max-min) + min
}
