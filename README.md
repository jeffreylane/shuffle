# shuffle

A simple package that shuffles slices of any type, similar to shuffling a deck of cards.