package main

import (
	"fmt"

	"gitlab.com/jeffreylane/shuffle"
)

func main() {
	arr := make([]int, 52)
	for i := 0; i < 52; i++ {
		arr[i] = i + 1
	}
	fmt.Println("Before shuffling")
	fmt.Println(arr)
	fmt.Println("After shuffling")
	fmt.Println(shuffle.Shuffle(arr))
}
